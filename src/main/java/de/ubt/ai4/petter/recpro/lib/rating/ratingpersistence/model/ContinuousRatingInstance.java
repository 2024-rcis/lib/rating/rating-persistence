package de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model;

import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.RatingType;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@DiscriminatorValue("CONTINUOUS")
public class ContinuousRatingInstance extends RatingInstance {

    public ContinuousRatingInstance(Long id, String recproElementId, String recproElementInstanceId, String ratingId, RatingType ratingType, String recproProcessInstanceId, Long value, String userId) {
        super(id, recproElementId, recproElementInstanceId, ratingId, ratingType, recproProcessInstanceId, userId);
        this.value = value;
    }
    private Long value;
}
