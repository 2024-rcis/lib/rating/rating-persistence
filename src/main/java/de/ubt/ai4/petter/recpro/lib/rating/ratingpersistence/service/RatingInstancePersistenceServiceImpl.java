package de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.service;

import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class RatingInstancePersistenceServiceImpl implements IRatingInstancePersistenceService{

    private RatingInstanceRepository ratingInstanceRepository;

    @Override
    public List<RatingInstance> getAll() {
        return ratingInstanceRepository.findAll();
    }

    @Override
    public List<RatingInstance> getByBpmElementId(String bpmElementId) {
        return new ArrayList<>();
    }

    @Override
    public void save(RatingInstance instance) {
        ratingInstanceRepository.saveAndFlush(instance);
    }

    @Override
    public List<RatingInstance> getByUserIds(List<String> userIds) {
        return ratingInstanceRepository.getByUserIdIn(userIds);
    }
}
