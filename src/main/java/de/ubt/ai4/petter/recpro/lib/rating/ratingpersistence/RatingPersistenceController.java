package de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence;

import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.service.IRatingInstancePersistenceService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/recpro/rating/persistence")
@AllArgsConstructor
public class RatingPersistenceController {

    private final IRatingInstancePersistenceService ratingInstancePersistenceServiceImpl;

    @PostMapping("save")
    public void save(@RequestBody RatingInstance ratingInstance) {
        ratingInstancePersistenceServiceImpl.save(ratingInstance);
    }

    @GetMapping("getAll")
    public ResponseEntity<List<RatingInstance>> getAll() {
        return ResponseEntity.ok(ratingInstancePersistenceServiceImpl.getAll());
    }

    @GetMapping("getByUserIds")
    public ResponseEntity<List<RatingInstance>> getByUserIds(@RequestParam List<String> userIds) {
        return ResponseEntity.ok(ratingInstancePersistenceServiceImpl.getByUserIds(userIds));
    }

}
